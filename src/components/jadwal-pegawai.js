/*global gantt*/
import React, { Component } from 'react'
import 'dhtmlx-gantt'
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css'


export class Gantt extends Component {
    componentDidMount(){
        gantt.init(this.ganttContainer);
        gantt.parse(this.props.tasks)
    }

    render(){
        return <div
            ref={(input) => { this.ganttContainer = input }}
            style={{width: '100%', height: '100%'}}
        ></div>
    }
}


export default class JadwalPegawai extends Component{
    render(){
        let tasks = {
            data: [
                {id: 1, text: 'User Requirement', start_date: '15-04-2017', duration: 3, progress: 0.6},
                {id: 2, text: 'Design System', start_date: '16-04-2017', duration: 3, progress: 0.4}
            ],
            links: [
                {id: 1, source: 1, target: 2, type: '0'}
            ]
        };

        return <div style={{height: 400}}>
            <Gantt tasks={tasks}/>
        </div>
    }
}