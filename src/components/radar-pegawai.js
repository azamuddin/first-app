import React, {Component} from 'react'
import Radar from 'react-d3-radar'

export default class PegawaiStrengh extends Component {
    render() {
    let variables = [
      {key: 'resilience', label: 'Resilience'},
      {key: 'speed', label: 'Speed'},
      {key: 'strength', label: 'Strength'},
      {key: 'agility', label: 'Agility'},
      {key: 'defense', label: 'Defense Prowess'}
    ]
    
    let saya = {
      key: 'saya',
      label: 'Saya',
      values: {
        resilience: 10,
        speed: 10,
        strength: 10,
        agility: 10,
        defense: 10
      }
    }

    let average = {
      key: 'average',
      label: 'Rata-rata Orang',
      values: {
        resilience: 7,
        speed: 6,
        strength: 8
      }
    }

    let target = {
      key: 'target',
      label: 'Target',
      values: {
        resilience: 6,
        speed: 6,
        strength: 6
      }
    }

    return (
      <div className="App">
        <Radar
          width={500}
          height={500}
          padding={70}
          domainMax={10}
          data={{
            variables: variables,
            sets: [saya, average, target]
          }}
        />
      </div>
    );
  }
}