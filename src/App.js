import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import RadarPegawai from './components/radar-pegawai'
import JadwalPegawai from './components/jadwal-pegawai'

class App extends Component {

  render(){
    return <div>
      <RadarPegawai/>
      <JadwalPegawai/>
    </div>
  }
}

export default App;
